# repk8-wheeler-finance-demos

Replicate Willie Wheeler's Python-based Financial Visualization Jupyter Notebooks

### Inspiration: 

Willie Wheeler's excellent set of Jupyter-based Python tutorials on financial charting, e.g.:

* _**Technical analysis of asset prices with Python**_
  * Archived [here](https://archive.ph/wip/NZgop) (from Medium's PITA [paywall](https://medium.com/wwblog/technical-analysis-of-investment-prices-with-python-fe44fcdbceea))
  * ...itself inspired by Clifford Ang's: [Analyzing Financial Data and Implementing Financial Models Using R](https://www.amazon.com/Analyzing-Financial-Implementing-Springer-Economics/dp/3030641546)

### Installation & Activation:

1. Clone this repo to your localhost;
1. Setup the (project-scoped) virtual environment on your localhost:
   * E.g. on a Linux Mint (LM) CLI: `./setup.project.venv.bash`
1. Activate the virtual environment:
   * On LM: `source ./project-venv/bin/activate`
1. Run the Jupyter Notebook:
   * On LM: `jupyter notebook ch01-prices.ipynb`
     * (On any *NIX system, adding an optional `&` suffix will background the process.) 
1. When done:
   1. Terminate the (CLI-based) instance, e.g.: `ctrl-c` (use `fg` first if backgrounded).
   1. Exit the virtual environment with `deactivate`.


### Resources: 
* Willie Wheeler's original project [repo (on GitHub)](https://github.com/williewheeler/finance-demos)
* The `pandas-ta` [repo on GitHub](https://twopirllc.github.io/pandas-ta/)
  > _Technical Analysis Indicators - Pandas TA is an easy to use Python 3 Pandas Extension with 130+ Indicators_
  * Oddly enough, the [installation instructions](https://twopirllc.github.io/pandas-ta/#stable) call for: `pip install pandas_ta`
* The `mplfinance` [repo on GitHub](https://github.com/matplotlib/mplfinance#mplfinance)
  > `matplotlib` utilities for the visualization, and visual analysis, of financial data.

